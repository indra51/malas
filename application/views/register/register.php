<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
		<meta name="author" content="Coderthemes">

		<link rel="shortcut icon" href="assets/images/favicon_1.ico">

		<title>Register Aplikasi Sederhana</title>

		<!-- Plugins -->
		<link href="<?php echo base_url(). "assets/plugins/select2/css/" ?>select2.min.css" rel="stylesheet" type="text/css" />
		<!-- End Plugins -->

		<link href="<?php echo base_url(). "assets/css/" ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(). "assets/css/" ?>core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(). "assets/css/" ?>components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(). "assets/css/" ?>icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(). "assets/css/" ?>pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(). "assets/css/" ?>responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url(). "assets/js/" ?>modernizr.min.js"></script>

	</head>
	<body>

		<div class="account-pages"></div>
		<div class="clearfix"></div>
		
		
		<div class="container-alt">
			<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="wrapper-page signup-signin-page">
					<div class="card-box">
						<div class="panel-heading">
							<h3 class="text-center"> Welcome to <strong class="text-custom">Aplikasi Sederhana</strong></h3>
						</div>

										<center><h4><b>Sign Up</b></h4></center>
		
						<div class="panel-body">
							<div class="row">
								<form class="form-horizontal m-t-20" method="post" action="<?php echo base_url()."register/register_proses/" ?>" enctype="multipart/form-data">
									<div class="col-lg-6">
										<div class="p-20">
											<div class="form-group ">
												<div class="col-xs-12">
													<input class="form-control" type="text" name="first_name" required="" placeholder="First Name">
												</div>
											</div>

											<div class="form-group ">
												<div class="col-xs-12">
													<input class="form-control" type="text" name="last_name" required="" placeholder="Last Name">
												</div>
											</div>

											<div class="form-group ">
												<div class="col-xs-12">
													<input class="form-control" type="text" name="phone" required="" placeholder="Phone">
												</div>
											</div>
										</div>
									</div>
								
									<div class="col-lg-6">
										<div class="p-20">

											<div class="form-group">
												<div class="col-xs-12">
													<input class="form-control" id="txtPassword" name="password" type="password" required="" placeholder="Password">
												</div>
											</div>

											<div class="form-group">
												<div class="col-xs-12">
													<input class="form-control" id="txtConfirmPassword" name="password" type="password" required="" placeholder="Confirm Password">
												</div>
											</div>

											<div class="form-group ">
												<div class="col-xs-12">
													<input class="form-control" name="email" type="email" required="" placeholder="Email">
												</div>
											</div>

											<div class="form-group">
												<div class="col-xs-12">
													<label class="control-label">Photo Profile</label>
													<input name="photo_profile" type="file" class="filestyle" data-input="false">
												</div>
											</div>
					
											<div class="form-group">
												<div class="col-xs-12">
													<div class="checkbox checkbox-primary">
														<input id="checkbox-signup" type="checkbox" checked="checked">
														<label for="checkbox-signup">I accept <a href="#">Terms and Conditions</a></label>
													</div>
												</div>
											</div>
					
											<div class="form-group text-right m-t-20 m-b-0">
												<div class="col-xs-12">
													<button class="btn btn-pink text-uppercase waves-effect waves-light w-sm" type="submit" id="btnSubmit">
														Sign Up
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		

		<script>
			var resizefunc = [];
		</script>

		<!-- jQuery  -->
        <script src="<?php echo base_url(). "assets/js/" ?>jquery.min.js"></script>
        <script src="<?php echo base_url(). "assets/js/" ?>bootstrap.min.js"></script>
        <script src="<?php echo base_url(). "assets/js/" ?>detect.js"></script>
        <script src="<?php echo base_url(). "assets/js/" ?>fastclick.js"></script>
        <script src="<?php echo base_url(). "assets/js/" ?>jquery.slimscroll.js"></script>
        <script src="<?php echo base_url(). "assets/js/" ?>jquery.blockUI.js"></script>
        <script src="<?php echo base_url(). "assets/js/" ?>waves.js"></script>
        <script src="<?php echo base_url(). "assets/js/" ?>wow.min.js"></script>
        <script src="<?php echo base_url(). "assets/js/" ?>jquery.nicescroll.js"></script>
        <script src="<?php echo base_url(). "assets/js/" ?>jquery.scrollTo.min.js"></script>

        <!-- Plugins -->
        <script src="<?php echo base_url(). "assets/plugins/select2/js/" ?>select2.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(). "assets/plugins/bootstrap-select/js/" ?>bootstrap-select.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(). "assets/plugins/bootstrap-filestyle/js/" ?>bootstrap-filestyle.min.js" type="text/javascript"></script>
        <!-- End Plugins -->

        <script src="<?php echo base_url(). "assets/js/" ?>jquery.core.js"></script>
        <script src="<?php echo base_url(). "assets/js/" ?>jquery.app.js"></script>

        <script type="text/javascript">
	        $(function () {
	            $("#btnSubmit").click(function () {
	                var password = $("#txtPassword").val();
	                var confirmPassword = $("#txtConfirmPassword").val();
	                if (password != confirmPassword) {
	                    alert("Passwords do not match.");
	                    return false;
	                }
	                return true;
	            });
	        });
	    </script>

	</body>
</html>