<!DOCTYPE html>
<html>
<!-- Start Head -->
  <?php echo $head; ?>
<!-- End Head -->

<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

            <!-- Top Bar Start -->
              <?php echo $top_bar; ?>
            <!-- Top Bar End -->


            <!-- Left Sidebar Start -->
              <?php echo $left_sidebar; ?>
            <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title"><b>Form Pesanan</b></h4>
                                    
                                    <div class="row">
                                    <form method="post" action="<?php echo base_url()."vtweb/vtweb_checkout" ?>">
                                        <div class="col-md-6">
                                            <div class="p-20">
                                                <h5><b>Company Name</b></h5>
                                                <?php $i = 1; ?>
                                                <?php foreach($this->cart->contents() as $items) { ?>
                                                    <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>
                                                <input type="text" class="form-control" name="company_name" required="" />
                                                
                                                <div class="m-t-20">
                                                    <h5><b>Email</b></h5>
                                                    <input type="text" name="email" class="form-control" value="<?php echo $user->email; ?>" required="" />
                                                </div>
                                                
                                                <div class="m-t-20">
                                                    <h5><b>Handphone</b></h5>
                                                    <input type="text" class="form-control" name="handphone" value="<?php echo $user->phone; ?>" required=""/>
                                                </div>

                                                <div class="m-t-20">
                                                    <h5><b>Jumlah Hari</b></h5>
                                                    <h6>Untuk Jumlah harinya, jika diubah maka harga juga berubah. Perhitungannya Jumlah hari dibagi 30 Di kali dengan jumlah harga</h6>
                                                    <select class="form-control select2" name='qty'>
                                                        <option><?php echo $items['qty'] * 30; ?> Hari</option>
                                                        <option value="30">30 Hari</option>
                                                        <option value="60">60 Hari</option>
                                                        <option value="90">90 Hari</option>
                                                        <option value="120">120 Hari</option>
                                                        <option value="150">150 Hari</option>
                                                        <option value="180">180 Hari</option>
                                                        <option value="210">210 Hari</option>
                                                        <option value="240">240 Hari</option>
                                                        <option value="270">270 Hari</option>
                                                        <option value="300">300 Hari</option>
                                                        <option value="330">330 Hari</option>
                                                        <option value="360">360 Hari</option>
                                                    </select>
                                                </div>
                                                <?php $i++; ?> 
                                                <?php } ?>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="p-20">

                                                <div class="m-t-20">
                                                    <h5><b>Total Harga</b></h5>
                                                    <input type="text" class="form-control" name="ubah_aja_gw_protek" value="Rp <?php echo $this->cart->format_number($this->cart->total()); ?>" readonly=""/>
                                                </div>

                                                <div class="m-t-20">
                                                    <h5><b>Keterangan</b></h5>
                                                    <textarea id="textarea" class="form-control" rows="2" name="keterangan" placeholder="This textarea has a limit of 255 chars."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group text-right m-t-20 m-b-0">
                                            <div class="col-xs-12">
                                                <button class="btn btn-pink text-uppercase waves-effect waves-light w-sm" type="submit" id="btnSubmit">
                                                  Submit Informations
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                <!-- end row -->
              


            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer">
            © 2016. All rights reserved.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <!-- Right Sidebar -->
    <div class="side-bar right-bar nicescroll">
        <h4 class="text-center">Chat</h4>
        <div class="contact-list nicescroll">
            <ul class="list-group contacts-list">
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-1.jpg" alt="">
                        </div>
                        <span class="name">Chadengle</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-2.jpg" alt="">
                        </div>
                        <span class="name">Tomaslau</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-3.jpg" alt="">
                        </div>
                        <span class="name">Stillnotdavid</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-4.jpg" alt="">
                        </div>
                        <span class="name">Kurafire</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-5.jpg" alt="">
                        </div>
                        <span class="name">Shahedk</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-6.jpg" alt="">
                        </div>
                        <span class="name">Adhamdannaway</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-7.jpg" alt="">
                        </div>
                        <span class="name">Ok</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-8.jpg" alt="">
                        </div>
                        <span class="name">Arashasghari</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-9.jpg" alt="">
                        </div>
                        <span class="name">Joshaustin</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-10.jpg" alt="">
                        </div>
                        <span class="name">Sortino</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
            </ul>
        </div>
    </div>
    <!-- /Right-bar -->


</div>
<!-- END wrapper -->

<!-- Start Javascript -->
  <?php echo $javascript; ?>
<!-- End Javascript -->

</body>
</html>