<!DOCTYPE html>
<html>
    <!-- Start Head -->
      <?php echo $head; ?>
    <!-- End Head -->


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
              <?php echo $top_bar; ?>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
              <?php echo $left_sidebar; ?>
            <!-- Left Sidebar End --> 



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="btn-group pull-right m-t-15">
                                    <a class="btn btn-default dropdown-toggle waves-effect waves-light" href="<?php echo base_url()."admin/post_pricing/" ?>"> Tambah Data </a>
                                </div>

                                <h4 class="page-title">Pricing Table</h4>
                                <ol class="breadcrumb">
                                    <li><a href="#">Ubold</a></li>
                                    <li><a href="#">Extras</a></li>
                                    <li class="active">Pricing Table</li>
                                </ol>
                            </div>
                        </div>

                    <div class="row pricing-plan">
                        <div class="col-md-12">
                            <div class="row">
                              <?php foreach($get_pricing as $data) { ?>
                                <div class="col-sm-6 col-md-6 col-lg-3">
                                    <div class="price_card text-center">
                                        <div class="pricing-header bg-primary">
                                            <span class="price">Rp<?php echo number_format($data['price']); ?></span>
                                            <span class="name"><?php echo $data['title']; ?></span>
                                        </div>
                                        <ul class="price-features">
                                            <li><?php echo $data['max_employee']; ?> Max Employee</li>
                                            <li><?php echo $data['max_admin']; ?> Max Admin</li>
                                            <li><?php echo $data['days']; ?> Days</li>
                                            <li>Created By <?php echo $data['created_by']; ?></li>
                                        </ul>
                                        <a class="btn btn-primary waves-effect waves-light w-md" href="<?php echo base_url()."admin/edit_pricing/$data[id_pricing]" ?>"> Edit Data </a>
                                    </div> <!-- end Pricing_card -->
                                </div> <!-- end col -->
                              <?php } ?>  
                            </div> <!-- end row -->
                        </div> <!-- end Col-10 -->
                    </div> <!-- end row -->
        		      </div> <!-- container -->
                               
                </div> <!-- content -->

                <footer class="footer">
                    © 2016. All rights reserved.
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar nicescroll">
                <h4 class="text-center">Chat</h4>
                <div class="contact-list nicescroll">
                    <ul class="list-group contacts-list">
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-1.jpg" alt="">
                                </div>
                                <span class="name">Chadengle</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-2.jpg" alt="">
                                </div>
                                <span class="name">Tomaslau</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-3.jpg" alt="">
                                </div>
                                <span class="name">Stillnotdavid</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-4.jpg" alt="">
                                </div>
                                <span class="name">Kurafire</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-5.jpg" alt="">
                                </div>
                                <span class="name">Shahedk</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-6.jpg" alt="">
                                </div>
                                <span class="name">Adhamdannaway</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-7.jpg" alt="">
                                </div>
                                <span class="name">Ok</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-8.jpg" alt="">
                                </div>
                                <span class="name">Arashasghari</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-9.jpg" alt="">
                                </div>
                                <span class="name">Joshaustin</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-10.jpg" alt="">
                                </div>
                                <span class="name">Sortino</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /Right-bar -->


        </div>
        <!-- END wrapper -->

        <!-- JavaScript -->
          <?php echo $javascript; ?>
        <!-- End JavaScript -->

	</body>
</html>