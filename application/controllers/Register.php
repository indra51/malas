<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	function __construct() 
    {
        parent::__construct();
 		$this->load->helper('url');
		
		$this->load->library('ion_auth');
		$this->load->library('uuid');
		$this->load->library('ciqrcode');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->auth = new stdClass;

		$this->load->model('admin_model');
	}

	function index()
	{
		if($this->ion_auth->logged_in())
		{
			redirect('client');
		}
		else
		{
			$this->load->view('register/register');
		}
	}

	function register_proses()
	{
    	$first_name = $this->input->post('first_name');
    	$last_name	= $this->input->post('last_name');
		$username 	= $first_name."".$last_name;
		$password 	= $this->input->post('password');
		$email 		= $this->input->post('email');
		$qr_code 	= $this->uuid->v5(date('Y-m-d H:i:s'));
		$additional_data = array(
								'first_name' 	=> $first_name,
								'last_name' 	=> $last_name,
								'phone'			=> $this->input->post('phone'),
							);

		$group = array('2'); // Sets user to admin.
		$active = false;

			$syg = $this->ion_auth->register($username, $password, $email, $additional_data, $group, $active);
			if($syg)
			{
				$this->session->set_flashdata('message', 'If you have access here, please login');
				redirect('login');
			}
			else
			{
				$this->session->set_flashdata('message', 'If you have access here, please login');
				redirect('register');
			}

	}
}
