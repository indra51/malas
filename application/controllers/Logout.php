<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	function __construct() 
    {
        parent::__construct();
		$this->load->library('session');
 		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->auth = new stdClass;
		
		$this->load->library('ion_auth');
	}

	function index()
	{
		$this->ion_auth->logout(TRUE);
		redirect('home/index');
	}
}
