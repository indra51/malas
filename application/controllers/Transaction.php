<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction extends CI_Controller {

	private $response;

	public function __construct()
    {
        parent::__construct();
        $params = array('server_key' => 'VT-server-vSiewVyK2i8kNPEUAc7dHMY2', 'sandbox' => false);
		$this->load->library('veritrans');
		$this->veritrans->config($params);
		$this->load->helper('url');

		//new Veritrans_Notification();

		$this->load->library('ion_auth');
		$this->load->library('uuid');
		$this->load->model('admin_model');	
		
    }

    public function index()
    {
    	$this->load->view('transaction');
    }

    public function service()
    {
    	$json_result = file_get_contents('php://input');
		$result = json_decode($json_result);

		if($result)
		{
			$notif = $this->veritrans->status($result->order_id);
		}
		error_log(print_r($result,TRUE));
		//notification handler sample
    }

    function finish()
    {
    	if($this->ion_auth->logged_in() != $this->ion_auth->is_admin())
		{	
			$get_order_id 			= $this->input->get('order_id');
			$get_transaction_status = $this->input->get('transaction_status');

			if($get_transaction_status == 'capture')
			{
				// get pesanan by code order
				$pesanan = $this->admin_model->get_pesanan_by_code_order($get_order_id)->result_array();
				foreach($pesanan as $order)
				{
					$data_start 	= $order['date'];
					$data_active 	= $order['jumlah_bulan'] * 30 + 1;
					$id_company		= $order['company_id'];
					$hasil_active	= $data_active;
					$name_package	= $order['title'];
					$max_employee	= $order['max_employee'];
					$max_admin		= $order['max_admin'];
				}
				$active = date('Y-m-d', strtotime($data_active.""."days", strtotime($data_start)));

				//Get id_user ion_auth 
				$user = $this->ion_auth->user()->row();

				//Get employee by id_user
				$employee = $this->admin_model->get_user_employee_by_id_user($user->id)->row();
				$employee_id = $employee->employee_id;
				
				$active_status = 1;

				//update employee
				$data_employee = array(
					'active_status' => $active_status,
				);
				$this->admin_model->update_employee($employee_id, $data_employee);

				//update company 
				$data_company = array(
					'active_until'			=> $active,
					'active_package_name'	=> $name_package,
					'max_employee'			=> $max_employee,
					'max_admin'				=> $max_admin,
					'tanggal_dibuat'		=> $data_start
				);
				$this->admin_model->update_setting_company($id_company, $data_company);

				// update enum
				$data_transaksi = array(
					'status' => 'Sudah dibayar'
				);

				$syg = $this->admin_model->update_enum_pesanan($get_order_id, $data_transaksi);
				if($syg)
				{
					redirect('client/invoice/'.$get_order_id);
					//echo "Status berhasil di rubah";
				}
				else
				{
					//echo "Status Gagal dirubah";
				}
			}
			else
			{
				echo "Terjadi Kesalahan saat pembayaran";
			}
    	}
    	else
    	{
    		$this->session->set_flashdata('message', 'If you have access here, please login');
			redirect('login_client');
    	}
    }

	public function status($order_id)
	{
		echo 'test get status </br>';
		print_r ($this->veritrans->status($order_id) );
	}

	public function cancel($order_id)
	{
		echo 'test cancel trx </br>';
		echo $this->veritrans->cancel($order_id);
	}

	public function approve($order_id)
	{
		echo 'test get approve </br>';
		print_r ($this->veritrans->approve($order_id) );
	}

	public function expire($order_id)
	{
		echo 'test get expire </br>';
		print_r ($this->veritrans->expire($order_id) );
	}
}
