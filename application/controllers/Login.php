<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	const base_url_login = 'https://api.kotamobagu.go.id/login.php';

	function __construct() 
    {
        parent::__construct();
 		$this->load->helper('url');
		
		$this->load->library('ion_auth');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->auth = new stdClass;
	}

	function index()
	{
		if($this->ion_auth->logged_in() != $this->ion_auth->is_admin())
		{
			redirect('client');
		}
		else
		{
			$this->load->view('login/login.html');
		}
	}

	function login_proses()
	{
		$identity = $_POST['username'];
		$password = $_POST['password'];
		$remember_user = true;
		$group = 'members';
		$group_id = $this->ion_auth->in_group($group);
	
		$syg = $this->ion_auth->login($identity, $password, $remember_user, $group_id);
		if($syg)
		{
			redirect('client');
		}
		else
		{
			$this->load->view('login_client/login.html');
		}
	}
}
