<?php
class Admin_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

    function input_pricing($data)
    {
        $insert = $this->db->insert('pricing', $data);
        return $insert;
    }

    function get_pricing()
    {
        $this->db->select('*');
        $this->db->from('pricing');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_pricing_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('pricing');
        $this->db->where('id_pricing', $id);
        return $this->db->get()->row();
    }

    function update_pricing($id, $data_update)
    {
        $this->db->where('id_pricing', $id);
        $this->db->update('pricing', $data_update);
        return true;
    }

    /* ==== Pesanan ===== */

    function insert_pesanan($data)
    {
        $insert = $this->db->insert('pesanan', $data);
        return $insert;
    }

    function get_pesanan_by_id_user($id)
    {
        $this->db->select('*');
        $this->db->from('pesanan');
        $this->db->where('user_id', $id);
        return $this->db->get();
    }

    function get_pesanan_by_code_order($id)
    {
        $this->db->select('*');
        $this->db->from('pesanan b');
        $this->db->join('pricing c', 'b.id_pricing = c.id_pricing', 'LEFT');
        $this->db->where('code_order', $id);
        return $this->db->get();
    }

    function get_invoice_by_code_order($id)
    {
        $this->db->select('*');
        $this->db->from('pesanan b');
        $this->db->join('pricing c', 'b.id_pricing = c.id_pricing', 'LEFT');
        $this->db->where('code_order', $id);
        return $this->db->get()->row();
    }

    function update_enum_pesanan($id, $data_update)
    {
        $this->db->where('code_order', $id);
        $this->db->update('pesanan', $data_update);
        return true;
    }

    function get_invoice_by_id_users($id)
    {
        $this->db->select('*');
        $this->db->from('pesanan b');
        $this->db->join('users c', 'b.user_id = c.id', 'LEFT');
        $this->db->join('pricing d', 'b.id_pricing = d.id_pricing', 'LEFT');
        $this->db->where('b.user_id', $id);
        return $this->db->get();
    }

    function get_invoice_code_order($code_order)
    {
        $this->db->select('*');
        $this->db->from('pesanan b');
        $this->db->join('users c', 'b.user_id = c.id', 'LEFT');
        $this->db->join('pricing d', 'b.id_pricing = d.id_pricing', 'LEFT');
        $this->db->where('b.code_order', $code_order);
        return $this->db->get();
    }

    // Setting Company //
    function input_setting_company($data)
    {
        $insert = $this->db->insert('setting_company', $data);
        return $insert;
    }

    function update_setting_company($id, $data_update)
    {
        $this->db->where('company_id', $id);
        $this->db->update('setting_company', $data_update);
        return true;
    }

    function get_company_by_id_company($company_id)
    {
        $this->db->select('*');
        $this->db->from('setting_company');
        $this->db->where('company_id', $company_id);
        return $this->db->get();
    }



    // function get_company_by_id_user()
    // {
    //     $this->db->select('*');
    //     $this->db->from('setting_company');
    //     $this->db->where('company_id', $company_id);
    //     return $this->db->get();
    // }

    function get_user_employee_by_id_user($id_user)
    {
        $this->db->select('*');
        $this->db->from('employee');
        $this->db->where('id_user', $id_user);
        return $this->db->get();
    }

    function get_employee_by_id_company($company_id)
    {
        $this->db->select('*, b.title as employee_title');
        $this->db->from('employee b');
        $this->db->join('setting_company c', 'b.company_id = c.company_id', 'LEFT');
        $this->db->join('department d', 'b.id_department = d.id_department', 'LEFT');
        $this->db->join('tunjangan e', 'b.id_tunjangan = e.id_tunjangan', 'LEFT');
        $this->db->where('b.company_id', $company_id);
        return $this->db->get();
    }

    // Employee //
    function input_employee($data)
    {
        $insert = $this->db->insert('employee', $data);
        return $insert;
    }

    function get_employee_by_id_user($id_user)
    {
        $this->db->select('*');
        $this->db->from('employee');
        $this->db->where('id_user', $id_user);
        return $this->db->get();
    }

    function get_karyawan()
    {
        $this->db->select('*');
        $this->db->from('employee c');
        $this->db->join('setting_company d', 'c.company_id = d.company_id', 'LEFT');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_karyawan_by_id_company($company_id)
    {
        $this->db->select('*');
        $this->db->from('employee');
        //$this->db->group_by('gaji_perjam');
        $this->db->where('company_id', $company_id);
        return $this->db->get();
    }

    function get_karyawan_by_id($id)
    {
        $this->db->select('*, b.title as employee_title');
        $this->db->from('employee b');
        $this->db->join('users c', 'b.id_user = c.id', 'LEFT');
        $this->db->join('tunjangan d', 'b.id_tunjangan = d.id_tunjangan', 'LEFT');
        $this->db->join('department e', 'b.id_department = e.id_department', 'LEFT');
        $this->db->join('setting_company f', 'b.company_id = f.company_id', 'LEFT');
        $this->db->where('employee_id', $id);
        return $this->db->get()->row();
    }

    function get_user_id_karyawan($email, $username) // Get user ion_auth di table users
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email', $email);
        $this->db->where('username', $username);
        return $this->db->get();
    }

    function update_employee($id, $data_update)
    {
        $this->db->where('employee_id', $id);
        $this->db->update('employee', $data_update);
        return true;
    }

    function delete_employee($employee_id)
    {
        $this->db->where('employee_id', $employee_id);
        $this->db->delete('employee'); 
    }

    // Department //
    function input_department($data)
    {
        $insert = $this->db->insert('department', $data);
        return $insert;
    }

    function get_department()
    {
        $this->db->select('*');
        $this->db->from('department');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_department_by_id_department($id_department)
    {
        $this->db->select('*');
        $this->db->from('department');
        $this->db->where('id_department', $id_department);
        return $this->db->get();
    }

    function update_department($id, $data_update)
    {
        $this->db->where('id_department', $id);
        $this->db->update('department', $data_update);
        return true;
    }

    function get_department_by_id_company($company_id)
    {
        $this->db->select('*');
        $this->db->from('department b');
        $this->db->join('setting_company c', 'b.company_id = c.company_id', 'LEFT');
        $this->db->where('b.company_id', $company_id);
        return $this->db->get();
    }

    function delete_department($id_department)
    {
        $this->db->where('id_department', $id_department);
        $this->db->delete('department'); 
    }

    // Tunjangan //
    function input_tunjangan($data)
    {
        $insert = $this->db->insert('tunjangan', $data);
        return $insert;
    }

    function get_tunjangan()
    {
        $this->db->select('*');
        $this->db->from('tunjangan');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_tunjangan_by_id_tunjangan($id_tunjangan)
    {
        $this->db->select('*');
        $this->db->from('tunjangan');
        $this->db->where('id_tunjangan', $id_tunjangan);
        return $this->db->get();
    }

    function get_tunjangan_by_id_company($company_id)
    {
        $this->db->select('*');
        $this->db->from('tunjangan b');
        $this->db->join('setting_company c', 'b.company_id = c.company_id', 'LEFT');
        $this->db->where('b.company_id', $company_id);
        return $this->db->get();
    }

    function update_tunjangan($id, $data_update)
    {
        $this->db->where('id_tunjangan', $id);
        $this->db->update('tunjangan', $data_update);
        return true;
    }

    function delete_tunjangan($id_tunjangan)
    {
        $this->db->where('id_tunjangan', $id_tunjangan);
        $this->db->delete('tunjangan'); 
    }

    // attendance atau absensi
    function input_absensi($data)
    {
        $insert = $this->db->insert('attendance', $data);
        return $insert;
    }

    function get_row_absensi_masuk($employee_id, $date_created)
    {
        $this->db->select('*');
        $this->db->from('attendance');
        $this->db->where('employee_id', $employee_id);
        $this->db->where('date_created', $date_created);
        $query = $this->db->get();
        return $query;
    }

    function input_update_absensi($id, $data_update)
    {
        $this->db->where('id_attendance', $id);
        $this->db->update('attendance', $data_update);
        return true;
    }

    function get_abseni_by_id_company($company_id, $employee_id)
    {
        $this->db->select('*');
        $this->db->from('attendance b');
        $this->db->join('employee c', 'b.employee_id = c.employee_id', 'LEFT');
        $this->db->where('b.company_id', $company_id);
        $this->db->where('b.employee_id', $employee_id);
        return $this->db->get();
    }

    function get_absensi_total_days($company_id, $employee_id)
    {
        $this->db->select('*, count(b.date_created) as total_days');
        $this->db->from('attendance b');
        $this->db->join('employee c', 'b.employee_id = c.employee_id', 'LEFT');
        $this->db->where('b.company_id', $company_id);
        $this->db->where('b.employee_id', $employee_id);
        $this->db->group_by('b.employee_id');
        return $this->db->get();
    }

    // function get_absensi_by_id_employee($employee_id)
    // {
    //     $this->db->select('*');
    //     $this->db->from('attendance');
    //     //$this->db->group_by('employee_id');
    //     $this->db->where('employee_id', $employee_id);
    //     return $this->db->get();
    // }

    // perhitungan laporan gaji 
    function get_company_by_id_company_for_laporan($company_id)
    {
        $this->db->select('*');
        $this->db->from('setting_company');
        $this->db->where('company_id', $company_id);
        return $this->db->get();
    }

    // function get_attendance_detail($company_id)
    // {
    //     $this->db->select('*');
    //     $this->db->from('attendance b');
    //     $this->db->join('employee c', 'b.employee_id = c.employee_id');
    //     $this->db->join('setting_company d', 'b.company_id = d.company_id');
    //     $this->db->where('b.company_id', $company_id);
    //     $query = $this->db->get();
    //     return $query->result_array();
    // }

    function laporan_gaji($company_id)
    {
        $this->db->select('*');
        $this->db->from('employee b');
        $this->db->join('attendance c', 'b.employee_id = c.employee_id', 'LEFT');
        $this->db->where('b.company_id', $company_id);
        $this->db->group_by('c.employee_id');
        return $this->db->get();
    }

    function get_employee_by_rate_hour($employee_id)
    {
        $this->db->select('*');
        $this->db->from('employee');
        //$this->db->group_by('employee_id');
        $this->db->where('employee_id', $employee_id);
        return $this->db->get();
    }
}    